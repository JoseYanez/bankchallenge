import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { Chart } from "chart.js";

@Component({
  selector: "app-chart-component1",
  templateUrl: "./chart-component1.component.html",
  styleUrls: ["./chart-component1.component.css"]
})
export class ChartComponent1Component implements OnInit {
  @Input() data: any[];
  chart: any[];
  charType: string;
  E_COLOR = "#ff2100";
  I_COLOR = "#0094ff";
  constructor() {
    this.charType = "bar";
  }

  ngOnInit() {
    this.createChart(this.charType);
  }

  createChart(_charType) {
    console.log(this.data);

    const dataEgreso = this.data["Egreso"];
    const dataIngreso = this.data["Ingreso"];
    const fechas = this.data["fechaStr"];
    const cntEgreso = dataEgreso.length;
    const cntIngreso = dataIngreso.length;
    const EgresoColor: any[] = Array(cntEgreso).fill("red");
    const IngresoColor: any[] = Array(cntIngreso).fill("blue");

    this.chart = new Chart("canvas", {
      type: _charType,
      data: {
        labels: fechas,
        datasets: [
          {
            label: "Egresos",
            data: dataEgreso,
            fill: false,
            backgroundColor: EgresoColor,
            borderColor: this.E_COLOR
          },
          {
            label: "Ingresos",
            data: dataIngreso,
            fill: false,
            backgroundColor: IngresoColor,
            borderColor: this.I_COLOR
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true
            }
          ],
          yAxes: [
            {
              display: true
            }
          ]
        }
      }
    });
  }

  changeTypeChart(type) {
    this.charType = type;
    this.createChart(this.charType);
  }
}
