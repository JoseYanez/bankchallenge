import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
/**
 * Importa bootstrap y jquery para habilitar carousel en html.
 */
import "bootstrap";
import $ from "jquery";
import { UsersService } from "../../services/users.service";
import { UserModel } from "../../models/users";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  users: UserModel[] = [];
  constructor(
    private _usersService: UsersService,
    private spinner: NgxSpinnerService
  ) {}
  
  ngOnInit() {
    // this.users = this._usersService.getUsers();
    // console.log(this.users);
    this.spinner.show();
    this._usersService.getUserFromAPI("").subscribe(response => {
      if (response) {
        // console.log(response.result);
        this.users = response.result;
      }
    });
    this.spinner.hide();
    // console.log(this._usersService.getUserFromAPI(""));
  }

  /**
   * Carga objeto JQUERY en DOM, luego que todo carga
   * para habilitar el funcionamiento de componente
   * carousel de bootstrap 4. para mostrar información de clientes.
   * 
   * @memberOf HomeComponent
   */
  ngAfterViewInit() {
    $(".carousel").carousel();
  }
}
