import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormControl, FormGroup } from "@angular/forms";
import { UsersService } from "../../services/users.service";
import { UserModel } from "../../models/users";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [UsersService]
})
export class LoginComponent implements OnInit {
  public user: UserModel;
  public status: string;
  public identity;
  public token;
  public title: string = "app";

  // exampleForm = new FormGroup({
  //   username: new FormControl(),
  //   password: new FormControl()
  // });
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _usersService: UsersService
  ) {
    this.title = "identificate";
    this.user = new UserModel("", "", "", "", "", "", "", "", 0);
  }

  ngOnInit() {}
  /**
   * Función que recibe datos de form de login de usuario.
   * Realiza la authentication del usuario con servicio
   * UsersService, antes de hacer login
   * verifica que no existan datos en localstorage
   * 
   * @param {any} form 
   * 
   * @memberOf LoginComponent
   */
  onSubmit(form) {
    this._usersService.signin(this.user).subscribe(
      response => {
        this.identity = response.result;
        if (!this.identity || !this.identity._id) {
          this.status = "error";
        } else {
          localStorage.setItem("identity", JSON.stringify(this.identity));
        }
        console.log(response.result.name);
        this.status = "success";
        this._router.navigate(["/dashboard"]);
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        if (errorMessage != null) {
          this.status = "error";
        }
      }
    );
  }
}
