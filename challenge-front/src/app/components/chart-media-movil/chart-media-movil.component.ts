import { Component, OnInit } from "@angular/core";
import { TransactionService } from "../../services/transaction.service";
import { Chart } from "chart.js";

@Component({
  selector: "app-chart-media-movil",
  templateUrl: "./chart-media-movil.component.html",
  styleUrls: ["./chart-media-movil.component.css"]
})
export class ChartMediaMovilComponent implements OnInit {
  dataChart: any[];
  chart: any[];
  chartMedia: any[];
  /**
   * Variable que hace de control para la carga del componente
   * evita que se cree sin datos.
   * @type {Promise<boolean>}
   * @memberOf ChartMediaMovilComponent
   */
  filtersLoaded: Promise<boolean>;
  /** 
   * @param {string} E_COLOR 
   * variable que manejan el color de Egresos
   * @param {string} I_COLOR 
   * variable que manejan el color de Ingresos
   * @param {string} N_COLOR 
   * variable que manejan el color de Neto
   * @param {string} M_COLOR 
   * variable que manejan el color de Media móvil
   *
   *  @memberOf ChartMediaMovilComponent
   */
  E_COLOR = "#ff2100";
  I_COLOR = "#0094ff";
  N_COLOR = "#1ea896";
  M_COLOR = "#4c5454";
  constructor(private _transactionService: TransactionService) {}

  ngOnInit() {
    this.loadDataChart();
  }

  /**
   * Función que obtiene datos para los graficos,
   * luego ejecuta cada creación de grafico con su función correspondiente
   * 
   * @memberOf ChartMediaMovilComponent
   */
  loadDataChart() {
    this._transactionService.getMediaMovil().subscribe(response => {
      if (response) {
        this.dataChart = response.result;
        this.createChart();
        this.filtersLoaded = Promise.resolve(true);
      }
    });
  }
  
  /**
   * 
   * 
   * 
   * @memberOf ChartMediaMovilComponent
   */
  createChart() {
    
    const dataEgreso: any[] = this.dataChart["Egreso"];
    const dataIngreso: any[] = this.dataChart["Ingreso"];
    const dataNeto: any[] = this.dataChart["neto"];
    // const dataMedia: any[] = this.dataChart["media"];
    const dataFechas: any[] = this.dataChart["fechaStr"];

    const dataMedia = this.calculaMediaMovil(dataNeto);

    const EgresoColor = Array(dataEgreso.length).fill(this.E_COLOR);
    const IngresoColor = Array(dataIngreso.length).fill(this.I_COLOR);
    const NetoColor = Array(dataNeto.length).fill(this.N_COLOR);
    const MediaColor = Array(dataMedia.length).fill(this.M_COLOR);

    this.createBarLineChart(
      dataFechas,
      dataMedia,
      MediaColor,
      dataEgreso,
      EgresoColor,
      dataIngreso,
      IngresoColor,
      dataNeto,
      NetoColor
    );
    this.createLineChart(dataFechas, dataMedia, MediaColor);
  }
  createLineChart(dataFechas: any[], dataMedia: any[], MediaColor: any[]) {
    this.chartMedia = new Chart("canvasLine", {
      type: "line",
      data: {
        labels: dataFechas,
        datasets: [
          {
            label: "Linea de media móvil",
            data: dataMedia,
            fill: false,
            backgroundColor: MediaColor,
            borderColor: this.E_COLOR
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true
            }
          ],
          yAxes: [
            {
              display: true,
              beginAtZero: true,
              labelString: "Transacciones Clientes (usd)"
            }
          ]
        }
      }
    });
  }

  createBarLineChart(
    dataFechas: any[],
    dataMedia: any[],
    MediaColor: any[],
    dataEgreso: any[],
    EgresoColor: any[],
    dataIngreso: any[],
    IngresoColor: any[],
    dataNeto: any[],
    NetoColor: any[]
  ) {
    this.chart = new Chart("canvas", {
      type: "bar",
      data: {
        labels: dataFechas,
        datasets: [
          {
            label: "Linea de media móvil",
            data: dataMedia,
            fill: false,
            backgroundColor: MediaColor,
            borderColor: this.M_COLOR,
            type: "line"
          },
          {
            label: "Egresos",
            data: dataEgreso,
            fill: false,
            backgroundColor: EgresoColor,
            borderColor: this.E_COLOR
          },
          {
            label: "Ingresos",
            data: dataIngreso,
            fill: false,
            backgroundColor: IngresoColor,
            borderColor: this.I_COLOR
          },
          {
            label: "Neto",
            data: dataNeto,
            fill: false,
            backgroundColor: NetoColor,
            borderColor: this.N_COLOR
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true
            }
          ],
          yAxes: [
            {
              display: true
            }
          ]
        }
      }
    });
  }

  /**
   * Función que calcula la media movil basandose en array de valores
   * netos de las fechas retornadas en la API
   * 
   * @param {any[]} neto 
   * @returns {any[]} 
   * 
   * @memberOf ChartMediaMovilComponent
   */
  calculaMediaMovil(neto: any[]): any[] {
    let first: number = neto[0];
    let media: any[] = [];

    for (let i = 0; i < neto.length; i += 1) {
      console.log(neto[i] + first);

      media.push(neto[i] + first);
    }

    return media;
  }
}
