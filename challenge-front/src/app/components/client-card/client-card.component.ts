import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-client-card",
  templateUrl: "./client-card.component.html",
  styleUrls: ["./client-card.component.css"]
})
export class ClientCardComponent implements OnInit {

  /**
   * 
   * 
   * @type user: any este valor se pasa entre un componente padre a uno hijo
   * para crear card de usuario en landing en bootstrap carousel
   * 
   * @memberOf ClientCardComponent
   */
  @Input() user: any;
  constructor() {}

  ngOnInit() {}
}
