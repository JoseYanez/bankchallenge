import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { TransactionService } from "../../services/transaction.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {

  groupby: string;
  dataChart: any[];
  /**
   * @any filtersLoaded se utiliza para el control de async y carga de otros componentes de la page
   * 
   * @type {Promise<boolean>}
   * @memberOf DashboardComponent
   */
  filtersLoaded: Promise<boolean>;

  /**
   * Constructor de componente que contempla _transactionService que
   * realiza el llamado al primer servicio, con los datos de Ingreso y Egreso de las transacciones de clientes.
   * @param {TransactionService} _transactionService servicio que obtiene datos del primer servicio
   * 
   * @param {NgxSpinnerService} spinner Servicio que muestra animación de carga
   * 
   * @memberOf DashboardComponent
   */
  constructor(
    private _transactionService: TransactionService,
    private spinner: NgxSpinnerService
  ) {
    this.groupby = "typeTxn,month,day";
  }

  ngOnInit() {
    this.loadDataChart(this.groupby);
  }

  /**
   * Función que ejecuta el llamado de base de datos.
   * Utiliza filtersLoaded: Promise<boolean> como control 
   * para crear componente de objeto chart js en componente hijo
   * @param {any} _groupby - parametro de agrupación de datos.
   * 
   * @memberOf DashboardComponent
   */
  loadDataChart(_groupby) {
    // Se muestra animación de carga mientras carga los datos
    this.spinner.show();
    this._transactionService.getTransaction(_groupby).subscribe(response => {
      
      if (response) {
        this.dataChart = response.result;
        console.log(this.dataChart);
        // una vez que termina el llamado a API se cambia el estado de la promesa
        this.filtersLoaded = Promise.resolve(true);
        this.spinner.hide();
      }
    });
  }
}
