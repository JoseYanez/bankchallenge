import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "@angular/forms";
import { routing, appRoutingProviders } from "./app.routes";
import { NgxSpinnerModule } from "ngx-spinner";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { TransactionComponent } from "./components/transaction/transaction.component";
import { HeaderComponent } from "./components/shared/header/header.component";
import { FooterComponent } from "./components/shared/footer/footer.component";
import { HomeComponent } from "./components/home/home.component";
import { AboutComponent } from "./components/shared/about/about.component";
import { PanelComponent } from "./components/panel/panel.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { ChartComponent1Component } from "./components/chart-component1/chart-component1.component";

// Services

import { TransactionService } from "./services/transaction.service";
import { UsersService } from "./services/users.service";
// import { ChartService1Service } from "./services/chart-service1.service";
import { ChartMediaMovilComponent } from "./components/chart-media-movil/chart-media-movil.component";
import { ClientCardComponent } from "./components/client-card/client-card.component";
import { SocketChartComponent } from './components/socket-chart/socket-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TransactionComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    PanelComponent,
    DashboardComponent,
    ChartComponent1Component,
    ChartMediaMovilComponent,
    ClientCardComponent,
    SocketChartComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule
  ],
  providers: [
    TransactionService,
    appRoutingProviders,
    UsersService,
    // ChartService1Service
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
