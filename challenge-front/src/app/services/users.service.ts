import { Injectable } from "@angular/core";
import { UserModel } from "../models/users";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { GLOBAL_BACKEND, TOKEN_SERVER } from "../global";



@Injectable({
  providedIn: "root"
})
export class UsersService {
  url: string;
  public identity;
  private endpoint: string = "users";
  
  
  /**
   * Creates an instance of UsersService.
   * @param {HttpClient} _httpClient 
   * 
   * @memberOf UsersService
   */
  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL_BACKEND;
  }

  /**
   * función que obtiene información completa de un usuario en base de datos.
   * 
   * @param {string} user
   * parametro que representa el nombre usuario
   * @returns {Observable<any>} 
   * 
   * @memberOf UsersService
   */
  getUserFromAPI(user: string): Observable<any> {
    let header = new HttpHeaders();
    const endPoint = `${this.url}/${this.endpoint}/${user || ""}`;

    header.set("Content-Type", "application/json");
    header.set("accept", "application/json");
    // header.set("Authentication", TOKEN_SERVER);
    let data = this._httpClient.get(endPoint, { headers: header });
    return data;
  }

  /**
   * Envia User Object para realizar sign in con backend
   * 
   * @param {any} user 
   * @returns {Observable<any>} 
   * 
   * @memberOf UsersService
   */
  signin(user): Observable<any> {
    // if(gettoken != null){
    //   user.gettoken = gettoken;

    // }
    let params = JSON.stringify(user);
    let header = new HttpHeaders();
    header.set("Content-Type", "application/json");
    header.set("accept", "application/json");
    // header.set("Authorization", TOKEN_SERVER);
    return this._httpClient.post(this.url + "/login/", params, {
      headers: header
    });
  }

  /**
   * Obtiene el objecto usuario de localStorage para realizar verificación de authentication
   * 
   * @returns 
   * 
   * @memberOf UsersService
   */
  getIdentity() {
    let identity = JSON.parse(localStorage.getItem("identity"));
    if (identity != null) {
      this.identity = identity;
    } else {
      this.identity = null;
    }
    return this.identity;
  }
}

// export interface User {
//   nombre: string;
//   bio: string;
//   img: string;
//   aparicion: string;
//   casa: string;
// }
