import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SOCKET_SERVER } from "../global";
import { ThrowStmt } from "@angular/compiler";

@Injectable()
export class SocketService {
    socketUrl: string;
    ws: WebSocket;
    
    constructor(){
        this.socketUrl = SOCKET_SERVER;
    }

    // createSocketConnection(): Observable<any>{
    //     this.ws = new WebSocket(this.socketUrl);
    //     return new Observable(observer => {
    //         this.ws.onmessage = (event) => observer.next(event.data);
    //         this.ws.onerror = (event) => observer.error(event);
    //         this.ws.onclose = (event) => observer.complete();
    //     });
        
    // }

    // sendMessage(message: any){
    //     this.ws.send(message);
    // }
}