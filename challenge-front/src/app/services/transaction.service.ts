import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { GLOBAL_BACKEND, TOKEN_SERVER } from "../global";

@Injectable()
export class TransactionService {
  public url: string;
  private endpoint: string = "clienttransaction";
  private endpointService2: string = "visualization";
  private transactions: Transaction[];
  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL_BACKEND;
  }

  getTransaction(groupby, clientId = ""): Observable<any> {
    let header = new HttpHeaders();
    const endPoint = `${this.url}/${this.endpoint}/${clientId ||
      ""}?init=2015-01-01&txnType=a&end=2018-12-31&limit=10000001`;

    header.set("Content-Type", "application/json");
    header.set("accept", "application/json");
    header.set("Authentication", TOKEN_SERVER);  
      let data = this._httpClient.get(endPoint, { headers: header });
      return data;
  }
  getMediaMovil(): Observable<any> {
    let header = new HttpHeaders();
    const endPoint = `${this.url}/${this.endpointService2}/`;

    header.set("Content-Type", "application/json");
    header.set("accept", "application/json");
    header.set("Authentication", TOKEN_SERVER);
    let data = this._httpClient.get(endPoint, { headers: header });
    return data;
  }
}

export interface Transaction {
  fecha: Date;
  clientId: number;
  txn: number;
}
