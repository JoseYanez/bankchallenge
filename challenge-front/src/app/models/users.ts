export class UserModel {
  constructor(
    public _id: string,
    public name: string,
    public image: string,
    public bio: string,
    public aparicion: string,
    public casa: string,
    public password: string,
    public role: string,
    public enable: number
  ) {}
}
