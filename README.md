# BankChallenge

---
Author: José Yáñez
Versión 1.0

---

Desafío de programación de Banistmo.
---

En el repositorio hay dos carpetas, una con el **backend **con una carpeta con el mismo nombre. y la otra contiene Frontend en una carpeta llamada **challenge-front**.

Se desarrolla backend con python utilizando `Tornado Framework` para utilización de procesos **asincronos** y mantenimiento de servidor **non-blocking**

Para poder ejecutar **Servidor Tornado** primero se debe de instalar los paquetes, con el commando `pip3 install -r requirements.txt`

Luego, para iniciar el servidor se utiza `python3 ChallengeServer.py`.

El **Servidor Tornado** tiene dos modos para la conexión a base de datos. En este caso se utiliza una base de datos MongoDB.

Por defecto está ejecutando el modo `cloud` que consume una base de datos de **mlab** para extraer información. Lo que permite obviar la instalación de mongodb en la maquina para revisión.

Segundo modo es `local` este requiere de una instalación de MongoDB local e importar la basde de datos.

### Configuración de modo cloud a local

Primero la base de dato se encuentra en la raiz del repositorio una copia de la base de datos, archivo backup con el nombre de `challengedbv2.agz`.

* backend
\ config.py
	- CONFIG_VALUES => diccionario (dict) - global - source = local


NOTA: El backend tiene las credenciales para el modo cloud, configurar de forma manual en caso de tener MongoDB local.


**Frontend**
---

El frontend está desarrollado en Angular 6 como framework web de javascript utilizando typescript.

Para ejecuta se debe utiliza el comando `npm install`, luego para hacer funcionar el proyecto se usa **npm start**


Detalle de ejecución
---

- Servidor Tornado: se ejecuta en puerto 3500
- Angular Application: Angular(js) 6 utiliza puerto 4200













