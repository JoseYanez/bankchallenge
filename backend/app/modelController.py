from motor.motor_tornado import MotorDatabase
from bson import ObjectId

# reference: https://gist.github.com/h0rn3t/02fc5e5ecaa8da73e5a64987c0b167e6

class model():

    def __init__(self, _db : MotorDatabase):
        db = _db

    def retrieve(self, nameCollection :str, _id):
        pass
    def retrieveWithFilter(self, nameCollection :str, pipeline: list):
        data = []
        cursor = self.db.get_collection(nameCollection).aggregate(pipeline)
        print(cursor)
        while (yield cursor.fetch_next):
            doc = cursor.next_object()
            print(doc)
            # doc['fecha'] = doc['fecha']
            data.append(doc)
        cursor.close()
        print(data)
        return data

    def save(self, nameCollection :str, dataObject :dict):
        data = {}
        if dataObject.get('_id', None) is not None:
            data = self.db.get_collection(nameCollection).insert_one(dataObject)
        else: # PUT verb - probably i will don't use this method
            data = self.db.get_collection(nameCollection).find_one_and_update(
                { "_id": ObjectId(dataObject.get('_id')) },
                {
                    '$set': dataObject
                }
            )
        return data
    
    def remove(self, nameCollection :str, dataObject :dict):
        if dataObject.get('_id', None) is not None:
            self.db.get_collection(nameCollection).remove({"_id": ObjectId(self._id)})
            self.clear()