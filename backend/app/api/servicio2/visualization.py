import json
from concurrent.futures import ThreadPoolExecutor
from datetime import date, datetime

import pandas as pd
import numpy as np
from tornado import gen
from tornado.concurrent import run_on_executor
from app.api.utils import BaseHandler, Utils

thread_pool = ThreadPoolExecutor(10)
VALID_LIST = ['typeTxn', 'month', 'day']
MONTHS = Utils().MONTHS


class VisualizationHandler(BaseHandler):
    """
    **VisualizationHandler** - clase que maneja 'visualizarion' ENDPOINT para el calculo de media movil
    """
    SUPPORTED_METHODS = ('GET')
    COLLECTION_NAME = 'transactions'
    _thread_pool = thread_pool

    @gen.coroutine
    def get(self, id):
        # print(self.request.headers)
        # if self.tokenValidator() == False:
        #     response = self.generic_response(
        #         [],
        #         400,
        #         'Incorrect or missing TOKEN in header(Authentication)!'
        #     )
        #     self.set_status(400)
        #     self.write_response(response)
        result = []
        db = self.settings['db']
        pipeline = yield self.getPipeline()
        cursor = db.get_collection(
            self.COLLECTION_NAME).aggregate(pipeline, allowDiskUse=True)
        while (yield cursor.fetch_next):
            doc = cursor.next_object()
            result.append(doc)
        cursor.close()
        data = yield self.processingData(result)
        print(data)
        result = Utils().genericResponse(data, 200, '')
        self.set_status(200)
        self.write_response(result)

    @run_on_executor(executor="_thread_pool")
    def processingData(self, data):
        """
        run_on_executor, decorador que permite la concurencia en la ejecución de esta función
        :data datos en formato de dict para procesar con pandas y retornar con formato para frontend
        """
        df = pd.DataFrame.from_dict(data)
        aux = df[['typeTxn', 'fechaStr', 'txn']].groupby(
            ['typeTxn', 'fechaStr']).aggregate(np.sum).reset_index()
        del df
        result = aux.pivot(index='fechaStr', columns='typeTxn',
                           values='txn').reset_index()
        result['Egreso'] = result['Egreso'].abs()
        result['neto_original'] = result['Ingreso'] - result['Egreso']
        result['neto'] = result['neto_original'].abs()

        return result.to_dict('list')

    @run_on_executor(executor="_thread_pool")
    def getPipeline(self):
        """
        Crea pipeline para MongoDB
        """
        result = []
        result.append({
            '$project': {
                '_id': 0,
                'fechaStr': 1,
                'typeTxn': 1,
                'txn': 1
            }
        })
        result.append({
            '$sort': {
                'fecha': 1
            },
        })

        return result
