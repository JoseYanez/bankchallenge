import json
from concurrent.futures import ThreadPoolExecutor
from datetime import date, datetime

import pandas as pd
import numpy as np
from tornado import gen
from tornado.concurrent import run_on_executor

from app.api.utils import BaseHandler, Utils


"""
ThreadPoolExecutor => permite hacer un listener que permita varios request
"""
thread_pool = ThreadPoolExecutor(10)
VALID_LIST = ['typeTxn', 'fechaStr']
MONTHS = Utils().MONTHS


class ClientTransactionHandler(BaseHandler):
    """ 
    .. ClientTransactionHandler:

        Clase que administra el ENDPOINT de `clienttransaction` que sólo permite el método
        GET para la extracción de datos mediante API RESTFUL

    """
    SUPPORTED_METHODS = ('GET')
    _thread_pool = thread_pool
    COLLECTION_NAME = 'transactions'

    @gen.coroutine
    def get(self, id):
        """

        """
        # self.set_default_headers()
        db = self.settings['db']
        query = {}

        query['init'] = self.get_query_arguments('init')[0]
        query['end'] = self.get_query_arguments('end')[0]
        query['txnType'] = self.get_query_arguments('txnType')[0]

        # print(self.get_query_arguments('groupby')[0])
        query['groupby'] = self.get_query_arguments('groupby')[0] if len(
            self.get_query_arguments('groupby')) > 0 else ''
        query['limit'] = self.get_query_arguments('limit')[0] if len(
            self.get_query_arguments('limit')) > 0 else ''
        pipeline, error = yield self.createAggregateFilter(id, query)
        if query['groupby'] != '':
            params = query['groupby'].split(',')
            for i in range(0, len(params)):
                if params[i].strip() not in VALID_LIST:
                    msg = 'invalid "{}" in groupby param'.format(params[i])
                    response = Utils().genericResponse([], 400, msg)
                    self.set_status(400)
                    self.write(response)
        if pipeline is None:
            response = {
                'status': 400,
                'error': error,
                'result': []
            }
            self.set_status(400)
        else:
            result = []
            print(pipeline)
            # TODO: docu allow disk use
            cursor = db.get_collection(self.COLLECTION_NAME).aggregate(
                pipeline, allowDiskUse=True)
            while (yield cursor.fetch_next):
                doc = cursor.next_object()
                result.append(doc)
            cursor.close()
            # if query['groupby'] != '':
            result = yield self.groupByFilter(result, 'typeTxn,fechaStr')
            response = Utils().genericResponse(result, 200, error)
            self.set_status(200)
        self.write_response(response)

    @run_on_executor(executor="_thread_pool")
    def groupByFilter(self, data: dict, options: str):
        params = options.split(',')
        params.append('txn')
        lenParams = len(params)
        print(lenParams)
        df = pd.DataFrame.from_dict(data)
        result = df[params].groupby(
            params[0: lenParams - 1]).aggregate(np.mean).reset_index()
        result = result.pivot(
            index='fechaStr', columns='typeTxn', values='txn').reset_index()

        # result['neto'] = result['Ingreso'] - result['Egreso'].abs()
        result['Egreso'] = result['Egreso'].abs()

        del df
        return result.to_dict('list')

    @run_on_executor(executor="_thread_pool")
    def createAggregateFilter(self, id: int, queryParams: dict):
        """ 
        Crea PIPELINE para ejecutar la función aggregate a MongoDB
        :queryParams - dict obtenido de URL de filtros en URL (query parameters)
        :id - CLIENT ID Permite filtrar por un CLIENT ID las transacciones en MongoDB(motor)
        """
        try:
            limit = self.settings['limit']
            existId = True if isinstance(id, int) or id is None else False
            countParams = 0
            txn = None
            clientId = None
            fecha = {'fecha': {}}
            if sum(map(len, queryParams.values())) == 0 and not existId:
                raise(
                    'invalid invoke. id and query parameters is not valid. Check and try again.')

            if queryParams.get('init', None) is not None:

                init = datetime.strptime(queryParams.get('init'), '%Y-%m-%d')
                countParams += 1

            if queryParams.get('end', None) is not None:
                end = datetime.strptime(queryParams.get('end'), '%Y-%m-%d')
                countParams += 1

            if queryParams.get('txnType', None) is not None:
                if queryParams.get('txnType', None) in ['i', 'e', 'a']:
                    txnType = queryParams.get('txnType')
                    countParams += 1

            try:
                if isinstance(int(id), int):
                    clientId = {'id': int(id)}
                    countParams += 1
            except:
                clientId = None
            if queryParams.get('limit', None) is not None:
                try:
                    # if isinstance(int(queryParams.get('limit')), int): # not works
                    if type(int(queryParams.get('limit'))) == int:
                        limit = int(queryParams.get('limit'))
                except:
                    return None, 'limit is valid.'
            print(limit)
            if countParams == 0:
                raise('In invoke not exists valid parameters. reject execution.')
            # { score: { $gt: 70, $lt: 90 } }

            if init or end:
                if init:
                    fecha['fecha']['$gt'] = init  # queryParams.get('init')
                if end:
                    fecha['fecha']['$lt'] = end  # queryParams.get('end')
            print(fecha)
            if txnType == 'i':
                txn = {
                    'typeTxn': 'Ingreso'
                }

            elif txnType == 'e':
                txn = {
                    'typeTxn': 'Egreso'
                }
            pipelineMatch = []
            if isinstance(clientId, dict):
                pipelineMatch.append(clientId)

            if isinstance(fecha, dict):
                pipelineMatch.append(fecha)

            if isinstance(txn, dict):
                pipelineMatch.append(txn)

            """
            **Objeto pipeline para MongoDB**

            - https://docs.mongodb.com/manual/aggregation/
            """
            pipeline = [{
                '$match': {
                    '$and': pipelineMatch
                }
            }, {
                '$project': {
                    '_id': 0,
                    'fecha': 1,
                    'fechaStr': 1,
                    'month': 1,
                    'day': 1,
                    'typeTxn': 1,
                    'txn': 1
                }
            }, {
                '$limit': limit
            }, {
                '$sort': {
                    'fecha': 1
                },
            }]
            return pipeline, ''
        except TypeError as tex:
            print(tex)
            return None, tex
        except Exception as ex:
            print(ex)
            return None, ex
