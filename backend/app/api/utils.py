import json
from datetime import date, datetime

from tornado import gen, web
from dicttoxml import dicttoxml
import Settings
# === Utils Class para todo el proyecto ===


class Utils():
    """
    Clase generica que estandariza funcionalidades comunes y rehusabilidad
    """
    MONTHS = {
        '1': 'enero',
        '2': 'febrero',
        '3': 'marzo',
        '4': 'abril',
        '5': 'mayo',
        '6': 'junio',
        '7': 'julio',
        '8': 'agosto',
        '9': 'septiembre',
        '10': 'octubre',
        '11': 'noviembre',
        '12': 'diciembre',
    }

    @staticmethod
    def genericResponse(data, status, errorMessage=''):
        """
        Crea objecto de respuesta generico
        """
        return {
            'status': status,
            'error': errorMessage,
            'result': data
        }

    @staticmethod
    def checkValue(value, controltype, empty=False, keyname=''):
        """
        Realiza un control/validación del valor de la variable, según el controltype.
        sí, key es distinto de blank, retorna un dict con keyname como key:valor
        """
        try:
            if isinstance(value, controltype):
                if empty and controltype == str:
                    if value == '':
                        return None

                if keyname != '':
                    return {
                        keyname: value
                    }
                else:
                    return value
            else:
                return None

        except TypeError as type_error:
            print('El valor {} no puede ser convertido a {}.'.format(
                str(value), str(controltype)))
            return None

        except Exception as ex:
            print(ex)
            return None

    @staticmethod
    def json_date_serial(obj):
        """JSON serializer for one object not serializable by default json code"""

        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        raise TypeError("Type %s not serializable" % type(obj))

    @staticmethod
    def json_serializer(obj):
        """JSON serializer for object not serializable by default json code"""
        return json.loads(json.dumps(obj, indent=4, sort_keys=True, default=str))

# === BaseHandler para extender las funcionalidades de Tornado ===


class BaseHandler(web.RequestHandler):
    """
    Extension de tornado
    """

    def set_default_headers(self):
        """
        Permite la activación del CORS en la API a través de un extender/modificar parametros en 'BaseHandler'
        Sólo se habilita GET y POST por seguridad. Actualmente, son los únicos http verbs utilizados.
        """
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", "GET,POST")
        self.set_header(
            "Access-Control-Allow-Headers",
            "Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, X-Requested-By, If-Modified-Since, X-File-Name, Cache-Control"
        )

    # def tokenValidator(self):
    #     valid = True
    #     print(self.request.headers.get('Authorization', 'no'))
    #     print(self.request.headers)
    #     header_token = self.request.headers.get('Authorization', '')
    #     if header_token != Settings.TOKEN:
    #         valid = False
    #     return valid
    #     # raise self. web.HTTPError(403, "Incorrect or missing password!")

    # @gen.coroutine
    def write_response(self, obj: dict, defaultJson=True):
        """
        Función que formatea 'obj' según valor de Accept.

        1. **json** 
        2. **xml**
        3. **javascript**

        **POR DEFECTO CONVIERTO TODO EN JSON**
        :obj - por lo general es un dict()
        :defaultJson - para disminuir fallas se utiliza json en 
        caso de desconocer el formato de salida/response
        """
        format = self.request.headers.get('Accept', 'json') or 'json'

        if format == 'json' or format == 'application/json':
            self.set_header("Content-Type", "application/json")
            if type(obj) == dict:
                self.write(obj)
            else:
                self.write(json.dumps(obj))
        elif format == 'javascript':
            self.set_header("Content-Type", "application/javascript")
            callback = self.get_argument('callback', 'callback')
            self.write('%s(%s);' % (callback, json.dumps(obj)))
        elif format == 'xml' or format == 'application/xml':
            self.set_header("Content-Type", "application/xml")
            self.write('<response>%s</response>' %
                       dicttoxml(obj).decode('utf-8'))
        elif defaultJson:
            if type(obj) == dict:
                self.write(obj)
            else:
                self.write(json.dumps(obj))
        else:
            raise web.HTTPError(
                400, 'Unknown response format requested: %s' % format)
