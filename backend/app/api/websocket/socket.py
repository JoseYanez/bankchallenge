import json
from concurrent.futures import ThreadPoolExecutor
from tornado import websocket
from tornado.concurrent import run_on_executor
from tornado import gen
import pandas as pd
from app.api.utils import BaseHandler, Utils

# import socketio


thread_pool = ThreadPoolExecutor(10)
COLLECTION_NAME = 'transactions'
# sio = socketio.AsyncServer(async_mode='tornado')

# @sio.on('')
# @gen.coroutine
# async def wakeUpAPI(self, sid):
#     await self.emit('pong_from_server', room=sid)


class WSHandler(websocket.WebSocketHandler):

    COLLECTION_NAME = 'transactions'
    _thread_pool = thread_pool

    def open(self):
        print('new connection')

    def on_message(self, message):
        print('message received:  %s' % message)
        # Reverse Message and send it back
        print('sending back message: %s' % message[::-1])
        self.write_message(message[::-1])

    def on_close(self):
        print('connection closed')

    def check_origin(self, origin):
        return True

    @run_on_executor(executor="_thread_pool")
    def processingData(self, data):
        df = pd.DataFrame.from_dict(data)
        aux = df[['typeTxn', 'fechaStr', 'txn']].groupby(
            ['typeTxn', 'fechaStr']).aggregate(np.sum).reset_index()
        del df
        result = aux.pivot(index='fechaStr', columns='typeTxn',
                           values='txn').reset_index()
        result['Egreso'] = result['Egreso'].abs()
        result['neto_original'] = result['Ingreso'] - result['Egreso']
        result['neto'] = result['neto_original'].abs()

        return result.to_dict('list')

    @run_on_executor(executor="_thread_pool")
    def getPipeline(self):
        result = []
        result.append({
            '$project': {
                '_id': 0,
                'fechaStr': 1,
                'typeTxn': 1,
                'txn': 1
            }
        })
        result.append({
            '$sort': {
                'fecha': 1
            },
        })

        return result
