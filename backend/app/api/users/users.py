# -*- coding: utf-8 -*-
from datetime import date, datetime
from tornado import web, escape, ioloop, httpclient, gen, escape
from concurrent.futures import ThreadPoolExecutor
from tornado.concurrent import run_on_executor
import json
from app.api.utils import Utils, BaseHandler
from urllib.parse import urlsplit, parse_qs

thread_pool = ThreadPoolExecutor(4)


class UserHandler(BaseHandler):
    SUPPORTED_METHODS = ('GET')
    _thread_pool = thread_pool
    COLLECTION_NAME = 'users'

    @gen.coroutine
    def get(self, id):
        # print(self.tokenValidator())
        # if self.tokenValidator() == False:
        #     response = Utils().genericResponse(
        #         [],
        #         400,
        #         'Incorrect or missing TOKEN in header(Authentication)!'
        #     )
        #     self.set_status(400)
        #     self.write_response(response)
        # print(id)
        name = Utils().checkValue(id, str, True, 'name')
        if name is None:
            name = {}

        db = self.settings['db']
        userObject = []
        userCursor = db.get_collection(
            self.COLLECTION_NAME
        ).find(name)
        # print(name)
        while (yield userCursor.fetch_next):
            aux = userCursor.next_object()
            aux['bio'] = aux['bio'].encode('ascii', 'ignore').decode('utf-8')
            aux.pop('_id', None)
            aux.pop('password', None)
            aux.pop('role', None)
            aux.pop('enable', None)
            # print(aux.values())
            userObject.append(Utils().json_serializer(aux))
        userCursor.close()
        # print(userObject)
        accept = self.request.headers['Accept']
        print(accept)
        result = Utils().genericResponse(userObject, 200, '')
        # if accept == '*/*' or accept == 'application/json':
        #      result = escape.json_encode(userObject)
        # if accept == 'application/xml':
        #     result = escape.xhtml_escape(result)

        self.set_status(200)
        self.write_response(result)
