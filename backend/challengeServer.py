import asyncio
import threading

import motor.motor_tornado
from tornado import auth, escape, httpserver, ioloop, options, web, websocket
from app.api.clientTransaction.clientTransaction import \
    ClientTransactionHandler
from app.api.servicio2.visualization import VisualizationHandler
# from app.api.websocket.socket import WSHandler
from app.api.users.login import LoginHandler
from app.api.users.users import UserHandler
from config import appConfig


conf = appConfig()
port = conf.getConfigByEnvironment('port')
address = conf.getConfigByEnvironment('address')
url, database = conf.getMongoUrl(conf.getConfigByEnvironment('source'))


class Application(web.Application):
    """
    Inicializador de configuración de servidor.
    **Hereda** - Todo las funcionalidades de `Tornado.web.Application`

    motor.motor_tornado.MotorClient() => driver de MongoDB que permite USO
    de co-rutinas y procesos asincronos
    """

    def __init__(self):
        print('init')
        print(url, database)
        print('-' * 20)
        # sio = socketio.tornado()
        #
        client = motor.motor_tornado.MotorClient(url)
        db = client[database]
        handlers = [
            (r'/clienttransaction/(.*)', ClientTransactionHandler),
            (r'/visualization/(.*)', VisualizationHandler),
            (r'/users/(.*)', UserHandler),
            (r'/login/(.*)', LoginHandler),
            # (r'/ws', WSHandler),
            # (r"/socket.io/", engineio.get_tornado_handler(sio)),
        ]
        settings = {
            'db': db,
            'MAX_WORKERS': 10,
            'limit': 50000,
            'debug': True
        }
        web.Application.__init__(self, handlers, **settings)


class ChallengeServer(threading.Thread):
    """
    Inicializardor de motor de Tornado Server.
    Componentes de servidor.

    1. **asyncio** - Permite ejecutar un nuevo 
    loop por cada request, conservando la caracteristica asynchronous de Tornado
    2. **HTTPServer** - Componente que permite la ejecución de servidor non-blocking

    """

    def run(self):
        try:
            if port is None or address is None:
                raise('you cannot run the server. check the config file')
            else:
                print(
                    'The full URL is http://{address}:{port}'.format(port=port, address=address))
                asyncio.set_event_loop(asyncio.new_event_loop())
                options.parse_command_line()
                app = Application()
                db = app.settings['db']
                # Non-blocking execution server
                server = httpserver.HTTPServer(app)
                server.listen(port)
                server.bind(8888)
                # myIP = socket.gethostbyname(socket.gethostname())
                # print('*** Websocket Server Started at %s***' % myIP)
                # server.start(0) # Not works with multiprocessing
                print(db)
                ioloop.IOLoop.current().start()
                print('-' * 20)
        except Exception as ex:
            print(ex)

# if __name__ == '__main__':
#     ChallengeServer.start()


ChallengeServer().start()
